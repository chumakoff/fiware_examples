module POI
  module ParkingTipsService
    def radial_search(lat, lon, opts = {})
      super(lat: lat, lon: lon, radius: opts[:radius] || 1000, category: 'parking_tip_active')
    end

    # Create Parking Tip POI
    def add_poi(tip)
      params = {
        fw_core: {
          location: {
            wgs84: {
              latitude: tip.lat,
              longitude: tip.lng
            }
          },
          categories: ['parking_tip', 'parking_tip_active', "parking_tip_#{tip.device_id}", "device_#{tip.device_id}"],
          name: {'en': tip.name.to_s},
          label: {'en': tip.address.to_s}
        }
      }

      params[:fw_media] = {
        entities: [
          {
            type: 'photo',
            caption: {'en': tip.name.to_s},
            url: "http://#{POIService::MEDIA_SERVER_IP}:8080#{tip.image_url}"
          }
        ]
      } if tip.image_url

      HTTParty.post("#{SERVER_URL}/add_poi", body: params.to_json, headers: headers)
    end

    # Update Parking Tip POI category
    def update_poi(uuid, active = true)
      current_attrs = get_pois(uuid, for_update: true)['pois'][uuid]
      current_categories = current_attrs['fw_core']['categories']
      category_to_add = "parking_tip_#{active ? 'active' : 'disabled'}"
      new_categories = (current_categories - ['parking_tip_active', 'parking_tip_disabled']) + [category_to_add]
      current_attrs['fw_core']['categories'] = new_categories

      params = {
        uuid => current_attrs
      }
      HTTParty.post("#{SERVER_URL}/update_poi", body: params.to_json, headers: headers)
    end

    def delete_poi(uuid)
      url = "#{SERVER_URL}/delete_poi?poi_id=#{uuid}"
      HTTParty.delete(url, headers: headers)
    end

    private

    def headers
      {'Content-Type' => 'application/json', 'Accept' => 'application/json'}
    end
  end
end
