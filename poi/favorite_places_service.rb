module POI
  module FavoritePlacesService
    def radial_search(lat, lon, opts = {})
      super(lat: lat, lon: lon, radius: opts[:radius] || 1000, category: 'favorite_place')
    end

    # Create Favorite Place POI
    def add_poi(place)
      params = {
        fw_core: {
          location: {
            wgs84: {
              latitude: place.lat,
              longitude: place.lng
            }
          },
          categories: ['favorite_place', "favorite_place_#{place.device_id}", "device_#{place.device_id}"],
          label: {"": place.name},
          name: {"": place.address},
          description: {"": "#{place.kind}/#{place.device_id}"}
        }
      }
      HTTParty.post("#{SERVER_URL}/add_poi", body: params.to_json, headers: headers)
    end

    # Update Favorite Place POI
    def update_poi(place_attrs)
      uuid = place_attrs[:uuid]
      device_id = place_attrs[:device_id]
      current_attrs = get_pois(uuid, for_update: true)['pois'][uuid]['fw_core']

      latitude = place_attrs[:lat] || current_attrs["location"]["wgs84"]["latitude"]
      longitude = place_attrs[:lng] || current_attrs["location"]["wgs84"]["longitude"]
      label = place_attrs[:name] || current_attrs["label"][""]
      name = place_attrs[:address] || current_attrs["name"][""]

      description = current_attrs["description"][""]
      timestamp = current_attrs['last_update']['timestamp']

      params = {
        uuid => {
          fw_core: {
            location: {
              wgs84: {
                latitude: latitude,
                longitude: longitude
              }
            },
            categories: ['favorite_place', "favorite_place_#{device_id}", "device_#{device_id}"],
            label: {"": label},
            name: {"": name},
            description: {"": description},
            last_update: {timestamp: timestamp}
          }
        }
      }
      HTTParty.post("#{SERVER_URL}/update_poi", body: params.to_json, headers: headers)
    end

    def delete_poi(uuid)
      url = "#{SERVER_URL}/delete_poi?poi_id=#{uuid}"
      HTTParty.delete(url, headers: headers)
    end

    private

    def headers
      {'Content-Type' => 'application/json', 'Accept' => 'application/json'}
    end
  end
end
