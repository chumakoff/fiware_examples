
# Export devices to Orion (creating)
def create_device(device)
  params = {
    type: 'Device',
    id: device_id(device),
    location: {
      value: "#{device.lat}, #{device.lng}",
      type: 'geo:point',
      metadata: {
        crs: {
          value: 'WGS84'
        }
      }
    }
  }
  HTTParty.post("#{BASE_URL}/entities/", body: params.to_json, headers: headers)
end

# Update devices in Orion
def update_device(device)
  params = {
    location: {
      value: "#{device.lat}, #{device.lng}"
    }
  }
  HTTParty.post("#{BASE_URL}/entities/#{device_id(device)}/attrs", body: params.to_json, headers: headers)
end

# Remove device
def delete(id)
  HTTParty.delete("#{BASE_URL}/entities/#{id}", headers: headers)
end

# Create TravelTime Recalculate (location leaving) subscription
def location_leaving_subscribe(device)
  params = {
    subject: {
      entities: [{id: "Device-#{device.id}"}],
      condition: {
        attrs: [],
        expression: {
          georel: "near;minDistance:100",
          geometry: 'point',
          coords: "#{device.lat}, #{device.lng}"
        }
      }
    },
    notification: {
      http: {
        url: "http://#{CALLBACK_IP}:8080/location_leaving"
      },
      attrs: ['id', 'location']
    },
    throttling: 5,
    expires: (Time.current + 1.year).strftime('%FT%TZ')
  }
  HTTParty.post("#{BASE_URL}/subscriptions/", body: params.to_json, headers: headers)
end

# Create Event Check-in (event coming) subscription
def event_coming_subscribe(event)
  params = {
    subject: {
      entities: [{id: "Device-#{event.device.id}"}],
      condition: {
        attrs: [],
        expression: {
          georel: "near;maxDistance:100",
          geometry: 'point',
          coords: "#{event.lat}, #{event.lng}"
        }
      }
    },
    notification: {
      http: {
        url: "http://#{CALLBACK_IP}:8080/event_coming"
      },
      attrs: ['id', 'location']
    },
    throttling: 5,
    expires: (event.end_date + 1.hour).strftime('%FT%TZ')
  }
  HTTParty.post("#{BASE_URL}/subscriptions/", body: params.to_json, headers: headers)
end

# Create Event Check-out (event leaving) subscription
def event_leaving_subscribe(event)
  params = {
    subject: {
      entities: [{id: "Device-#{event.device.id}"}],
      condition: {
        attrs: [],
        expression: {
          georel: "near;minDistance:100",
          geometry: 'point',
          coords: "#{event.lat}, #{event.lng}"
        }
      }
    },
    notification: {
      http: {
        url: "http://#{CALLBACK_IP}:8080/event_leaving"
      },
      attrs: ['id', 'location']
    },
    throttling: 5,
    expires: (event.end_date + 1.hour).strftime('%FT%TZ')
  }
  HTTParty.post("#{BASE_URL}/subscriptions/", body: params.to_json, headers: headers)
end

# Remove subscription
def remove_subscr(subscr_id)
  HTTParty.delete("#{BASE_URL}/subscriptions/#{subscr_id}", headers: headers)
end

# Create Home Check-in (home_coming) subscription
def home_coming_subscribe(home)
  params = {
    subject: {
      entities: [{id: "Device-#{home.device.id}"}],
      condition: {
        attrs: [],
        expression: {
          georel: "near;maxDistance:100",
          geometry: 'point',
          coords: "#{home.lat}, #{home.lng}"
        }
      }
    },
    notification: {
      http: {
        url: "http://#{CALLBACK_IP}:8080/home_coming"
      },
      attrs: ['id', 'location']
    },
    throttling: 5,
    expires: (Time.current + 1.year).strftime('%FT%TZ')
  }
  HTTParty.post("#{BASE_URL}/subscriptions/", body: params.to_json, headers: headers)
end

# Create Home Check-out (home_leaving) subscription
def home_leaving_subscribe(home)
  params = {
    subject: {
      entities: [{id: "Device-#{home.device.id}"}],
      condition: {
        attrs: [],
        expression: {
          georel: "near;minDistance:100",
          geometry: 'point',
          coords: "#{home.lat}, #{home.lng}"
        }
      }
    },
    notification: {
      http: {
        url: "http://#{CALLBACK_IP}:8080/home_leaving"
      },
      attrs: ['id', 'location']
    },
    throttling: 5,
    expires: (Time.current + 1.year).strftime('%FT%TZ')
  }
  HTTParty.post("#{BASE_URL}/subscriptions/", body: params.to_json, headers: headers)
end
