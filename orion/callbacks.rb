def event_coming
  remove_subscription
  save_coming_time
  subscribe_to_event_leaving
end

def event_leaving
  remove_subscription
  save_leaving_time
end

def location_leaving
  update_travel_time_to_next_event
  subscribe_to_location_leaving
end

def home_coming
  remove_subscription
  save_coming_time
  subscribe_to_home_leaving
end

def home_leaving
  remove_subscription
  save_leaving_time
  subscribe_to_home_coming
end
